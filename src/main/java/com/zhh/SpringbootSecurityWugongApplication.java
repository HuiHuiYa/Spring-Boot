package com.zhh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSecurityWugongApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecurityWugongApplication.class, args);
    }

}
