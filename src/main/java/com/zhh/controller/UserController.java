package com.zhh.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
    /**
     * 欢迎页面
     * @return
     */
    @RequestMapping("/")
    public String welcome(){
        return "welcome";
    }

    /**
     * 登录
     */
    @RequestMapping("/tologin")
    public String login(){
        return "pages/login";
    }

    /**
     * level1页面跳转
     */
    @RequestMapping("/level1/{path}")
    public String level1(@PathVariable String path){
        System.out.println("level3");
        return "pages/level1/"+path;
    }

    /**
     * level2页面跳转
     */
    @RequestMapping("/level2/{path}")
    public String level2(@PathVariable String path){
        System.out.println("level3");
        return "pages/level2/"+path;
    }

    /**
     * level3页面跳转
     */
    @RequestMapping("/level3/{path}")
    public String level3(@PathVariable String path){
        System.out.println("level3");
        return "pages/level3/"+path;
    }


}
