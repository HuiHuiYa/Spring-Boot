package com.zhh.mapper;

import com.zhh.pojo.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface MenuMapper {
    List<Menu> getAllMenus();
}
